# sw2ts-generate 
------

## How to lint

```sh
npx eslint --init

# "lint": "npx eslint --fix src/**"
```

## How to prettier

```sh
yarn add prettier-eslint

# https://astexplorer.net
# https://prettier.io/docs/en/options.html
# 当前解析器使用: babel-ts
# 可优先通过设置 .eslintrc.js 规则进行配置覆盖;
```


## ts-node
+ 尝试使用--files选项:
```json
"scripts": {
	"dev": "npx ts-node --files ./src/index.ts"
},
```
Note: [help-my-types-are-missing](https://github.com/TypeStrong/ts-node#help-my-types-are-missing)
默认情况下，ts-node在启动时不会从tsconfig.json加载files、include和exclude。



## TypeScript AST
+ [AST explorer](https://astexplorer.net)
+ [TypeScript AST viewer](https://ts-ast-viewer.com)
