# sw2ts-generate 
------

## Install

```sh
$ npm install -g sw2ts-generate
```

or, install with `--save-dev` option to use in [npm scripts](https://docs.npmjs.com/misc/scripts).

## Usage

```sh
Usage: sw2ts [options] [--] [filename]

Options:
  -o, --out <file>                output filename.
  -d, --dir <path>                output dirname.
  --url <url>                     input json schema from the url.
  -r, --required                  set property is required
  -c, --config <file>             set configuration file path.
  -p, --proxy <path>              set proxy path
  -n, --namespace <items...>      set namespace separated list
```

* *filename* should be swagger.json file.

### Example

```sh
# 解析在线 swagger.json
$ sw2ts -o output --url https://petstore.swagger.io/v2/swagger.json -r

# 解析本地 swagger.json
$ sw2ts -o output -d schema swagger.json

# 通过 -r, 强制指定属性全部必传
$ sw2ts -o output -d schema -r swagger.json

# 通过 -n, 指定命名空间，生成部分API
$ sw2ts -o output --url https://petstore.swagger.io/v2/swagger.json -r -n /pet /user/login
```

### Usage Tips

Use sw2ts in your script.

Code Sample:

```typescript
import * as ts from 'typescript';

import * as sw2dts from '../index';

// @ts-ignore
const callback  = (data: any) => sw2dts.default()(data);

// @ts-ignore
const compilerOptions = { module: ts.ModuleKind.ES2015 };

// @ts-ignore
function fecth (url: string, code: string) {
	sw2dts.setConfSw2ts({
		proxy: 'api',
		file: '',
		namespace: []
	});

	if (code === 'typescript') {
		// generate typescript code
		sw2dts.readSchemaFromUrl(url).then(r => sw2dts.Generate2API(r, 'mock'));
	} else {
		sw2dts.readSchemaFromUrl(url).then(callback).then(r => {
			// generate javascript code
			sw2dts.createTransfer({ compilerOptions })(r.paths, 'release/index.js');
		});
	}
}

// ts-node --files  .\src\tests\index.ts
fecth('https://petstore.swagger.io/v2/swagger.json', 'typescript');
```

## Format

[prettier](https://prettier.io/docs/en/options.html)、[prettier-eslint](https://www.npmjs.com/package/prettier-eslint)

```javascript
// 可以通过重写 .eslintrc.js  .prettierrc.js 实现自定义格式化效果
module.exports = {
	'singleQuote': true,
	'printWidth': 120,
	'prettier.tabWidth': 2,
	'prettier.semi': true,
	'proseWrap': 'never',
	'prettier.useTabs': false,
	'parser': 'babel-ts',
	'prettier.bracketSpacing': true,
}
```

## Notes

```typescript
/**
 * http://10.4.196.168:31621/v2/api-docs
 * path: /pet/{state.petId}
 * 本插件，示例如下:
 * 即将 body 中的参数，置于 payload 字段下，便于使用；
 * 由此可能导致请求参数命名不能出现 payload 名称，请注意；
 */
export function postPetPetId<T = any> (
	state: {
		payload?: FormData;
		petId: number;
	},
	config?: RequestConfig
) {
	return API.post<T>(`/api/pet/${state.petId}`, state.payload, config);
}

/**
 * 键名包含 id 的属性，通常是 number 类型，
 * 不好设置有效的默认值，故本插件统一重置为可选属性
 * 插件生成示例代码如下
 */
declare interface Order {
	id?: number; // int64
	petId?: number; // int64
	quantity: number; // int32
	shipDate: string; // date-time
	/* Order Status */
	status: OrderStatus;
	complete: boolean;
}
```

## How to build

```sh
npm run build
```
