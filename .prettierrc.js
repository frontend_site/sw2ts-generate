
module.exports = {
	'singleQuote': true,
	'printWidth': 120,
	'prettier.tabWidth': 2,
	'prettier.semi': true,
	'proseWrap': 'never',
	'prettier.useTabs': false,
	'parser': 'babel-ts',
	'prettier.bracketSpacing': true,
}