import ts = require('typescript');

type Transfer = (input: string, output: string) => void;

export function createTransfer(opts: ts.TranspileOptions): Transfer;

export function setConfSw2ts(config: ConfigSw2ts): void;

export function readSchemaFromFile(file: string): Promise<any>;

export function readSchemaFromUrl(url: string): Promise<any>;

export function Generate2API(data: SwaggerSpec, file?: string): void;

export default function GenerateContext(config?: ConfigSw2ts): CtxCall;

