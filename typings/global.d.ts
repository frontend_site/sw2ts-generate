interface Sw2tsType {
	namespace: string[];
	proxy: string;
	required: boolean;
	api: string;
	http: string;
	local: boolean;
	dts: string;
	hasContext: Set<string>;
	requireCtx: Set<string>;
}

interface AliasEnum {
	type?: string;
	name: string;
	property?: string;
	items: any[];
	description: string;
}

interface ReferenceSchema {
	schema: any;
}

interface SchemaItemFace {
	$ref: string;
	originalRef: string;
	type: string;
	items: {
		$ref: string;
		originalRef: string;
	};
}

interface EnumType {
	type: string;
	enum: any[];
	default: number | string;
}

interface ParameterFace {
	required: boolean;
	in: string;
	type: string;
	name: string;
	description: string;
	items?: EnumType;
	schema: SchemaItemFace;
}

interface ResponseProps {
	in: string;
	required: boolean;
	type: string;
	name: string;
	description: string;
	schema: SchemaItemFace;
}

interface SchemaContentFace {
	method: string;
	operationId: string;
	url: string;
	response: ResponseProps;
	summary: string;
	parameters: ParameterFace[];
}

interface ParamSpec {
	in: string;
	name: string;
	type: string;
	ref?: string;
	required?: boolean;
}

interface ParamStoreSpec {
	[key: string]: Record<string, ParamSpec>;
}

interface DefinitionSpec {
	__r__?: boolean;
	type: string;
	required?: string[];
	xml?: any;
	description?: string;
	properties: Record<string, any>;
}

interface SwaggerSpec {
	paths: any;
	definitions: Record<string, DefinitionSpec>;
}

type ConfigSw2ts = {
	proxy?: string;
	path?: string;
	required?: boolean;
	file: string;
	namespace: string[];
}

type CtxCall = (data: SwaggerSpec, aliasImport?: string) => {
	paths: any;
	definitions: any;
}

type AvailableParameter = { [x: string]: ParameterFace };

type GenerateFn = (ctx: SwaggerSpec) => any[];

type PathContentProps = { [x: string]: SchemaContentFace };

type DeclareNameProps = string[] | { name: string; alias: string }[];

declare namespace Sw2tsGen {
	type Optional<T, K extends keyof T> = Omit<T, K> & Partial<Pick<T, K>>;
}
