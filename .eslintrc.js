module.exports = {
	globals: { module: true },
	env: {
		browser: true,
		es2021: true
	},
	extends: ['eslint:recommended', 'plugin:@typescript-eslint/recommended'],
	parser: '@typescript-eslint/parser',
	parserOptions: {
		ecmaVersion: 12,
		sourceType: 'module'
	},
	plugins: ['@typescript-eslint', 'simple-import-sort'],
	rules: {
		camelcase: 'off',
		'simple-import-sort/imports': 'error',
		'simple-import-sort/exports': 'error',
		'no-unused-vars': 'off',
		'no-use-before-define': 'off',
		'valid-typeof': 'off',
		'no-useless-call': 'off',
		'space-before-function-paren': ['error', 'always'],
		'@typescript-eslint/ban-ts-comment': 0,
		'space-infix-ops': [
			'error',
			{ int32Hint: false },
		],
		'no-empty': [
			'error',
			{ allowEmptyCatch: true },
		],
		'max-len': [
			'error',
			{
				code: 120,
				comments: 600,
			},
		],
		'array-element-newline': ['error', 'consistent'],
		'comma-dangle': [
			'error',
			{
				objects: 'only-multiline',
				functions: 'never',
				arrays: 'only-multiline',
				imports: 'only-multiline',
				exports: 'only-multiline'
			},
		],
		'no-trailing-spaces': ['error'],
		'object-property-newline': ['error'],
		'object-curly-newline': [
			'error',
			{
				ObjectExpression: {
					multiline: true,
					minProperties: 2
				},
				ObjectPattern: {
					multiline: true,
					minProperties: 2
				},
				ImportDeclaration: {
					multiline: true,
					minProperties: 2
				},
			},
		],
		'object-curly-spacing': ['error', 'always'],
		'no-multiple-empty-lines': [
			'error',
			{ max: 2 },
		],
		'padding-line-between-statements': [
			'error',
			{
				blankLine: 'always',
				prev: 'var',
				next: 'return'
			},
		],
		'no-useless-rename': [
			'error',
			{
				ignoreImport: true,
				ignoreExport: true,
				ignoreDestructuring: true
			},
		],
		'semi-style': ['error', 'last'],
		indent: ['error', 'tab'],
		'linebreak-style': ['error', 'windows'],
		quotes: ['error', 'single'],
		semi: ['error', 'always'],
		'no-extra-boolean-cast': 'off',
		'no-async-promise-executor': 'off',
		'@typescript-eslint/ban-types': 'off',
		'@typescript-eslint/no-empty-function': 'off',
		'@typescript-eslint/no-explicit-any': 'off',
		'@typescript-eslint/no-var-requires': 'off',
		'@typescript-eslint/no-non-null-assertion': 'off',
		'@typescript-eslint/no-unused-vars': 'off',
		'@typescript-eslint/explicit-module-boundary-types': 'off',
	}
};
