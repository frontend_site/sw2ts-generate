export default `
import axios, { AxiosInstance } from 'axios';
import { AxiosRequestConfig as RequestConfig } from 'axios';

class API {
	https: AxiosInstance;
	constructor() {
		this.https = axios.create({
			baseURL: '/'
		});
	}
	fetch<T = any>(config: RequestConfig) {
		return this.https(config) as T;
	}
	get<T>(url: string, params: any, config?: RequestConfig): Promise<T> {
		throw new Error('Method not implemented.');
	}
	head<T>(url: string, params: any, config?: RequestConfig): Promise<T> {
		throw new Error('Method not implemented.');
	}
	put<T>(url: string, data: any, config?: RequestConfig): Promise<T> {
		throw new Error('Method not implemented.');
	}
	patch<T>(url: string, data: any, config?: RequestConfig): Promise<T> {
		throw new Error('Method not implemented.');
	}
	post<T>(url: string, data: any, config?: RequestConfig): Promise<T> {
		throw new Error('Method not implemented.');
	}
	delete<T>(url: string, params: any, config?: RequestConfig): Promise<T> {
		throw new Error('Method not implemented.');
	}
	static create() {
		return new API();
	}
}

export { RequestConfig };

export default API.create();`;