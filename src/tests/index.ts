import * as ts from 'typescript';

import * as sw2dts from '../index';

// @ts-ignore
const callback = (data: any) => sw2dts.default()(data);

// @ts-ignore
const compilerOptions = { module: ts.ModuleKind.ES2015 };

// @ts-ignore
function fecth (url: string, code: string) {
	sw2dts.setConfSw2ts({
		proxy: 'api',
		file: '',
		namespace: [],
	});

	if (code === 'typescript') {
		// generate typescript code
		sw2dts.readSchemaFromUrl(url).then((r) => sw2dts.Generate2API(r, 'mock'));
	} else {
		sw2dts
			.readSchemaFromUrl(url)
			.then(callback)
			.then((r) => {
				// generate javascript code
				sw2dts.createTransfer({ compilerOptions })(r.paths, 'release/index.js');
			});
	}
}

// ts-node --files  .\src\tests\index.ts
fecth('https://petstore.swagger.io/v2/swagger.json', 'javascript');
