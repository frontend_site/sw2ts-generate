export const isEndWiths = /^.+(\.ts)$/g;

export const methods = [
	'post',
	'put',
	'patch',
	'postForm',
	'putForm',
	'patchForm'
];

export const paramsInUrl = ['query', 'path'];

export const typeState = new Map<string, (v: string) => string>([
	['array', (_v: string) => _v + '[]'],
	['boolean', (_v: string) => 'boolean'],
	['integer', (_v: string) => 'number'],
	['null', (_v: string) => 'null'],
	['string', (_v: string) => 'string'],
]);

export const builtIn = [
	'Map«string,string»',
	'Map«string,object»',
	'Map«string,boolean»',
	'Map',
	'HashMap«string,string»'
];

export const localType = [
	'string',
	'number',
	'boolean',
	'bigint',
	'any',
	'FormData',
	'null',
	'undefined',
	'Record<string, any>'
];
