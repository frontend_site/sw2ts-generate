import {
	eslintConfig, prettierOptions
} from '../config';
import { isEndWiths } from '../constants';
const fs = require('fs');
const path = require('path');
const format = require('prettier-eslint');

export function dts2format (file = 'index', dir = 'example') {
	const sw2ts = isEndWiths.test(file) ? file.replace('.ts', '.d.ts') : `swagger/${file}.d.ts`;

	return path.join(dir, 'typings', sw2ts);
}

export function api2format (file = 'index', dir = 'example') {
	const sw2ts = isEndWiths.test(file) ? file : `api/${file}.ts`;
	return path.join(dir, 'service', sw2ts);
}

export function http2format (_file: string, dir = 'example') {
	return path.join(dir, 'service', 'request.ts');
}

export function initConfig (file: string) {
	const co = path.join(process.cwd(), file);
	if (fs.existsSync(co)) return require(co);
}

export function format2dts (source_code = '') {
	const options = {
		eslintConfig: initConfig('.eslintrc.js') || eslintConfig,
		text: source_code,
		fallbackPrettierOptions: { singleQuote: false },
		prettierOptions: initConfig('.prettierrc.js') || prettierOptions,
	};

	return format(options);
}
