const chalk = require('chalk');

function error (message: any) {
	console.log(chalk.red(message));
}

function warn (message: any) {
	console.log(chalk.yellow(message));
}

function info (message: any) {
	console.log(chalk.blue(message));
}

function success (message: any) {
	console.log(chalk.green(message));
}

export default {
	error,
	warn,
	info,
	success
};
