import * as _ from 'lodash';

import { GetSw2ts } from '../config';

export function handleSchema2JSON (paths: any): PathContentProps {
	const hasIncds = [] as string[];
	const opts = GetSw2ts();
	const dataSchema: PathContentProps = Object.create(null);

	for (const key in paths) {
		let prefix = key.split('/').splice(-3).join('/');
		if (hasIncds.includes(prefix)) {
			prefix = key.split('/').splice(-4).join('/');
		}
		hasIncds.push(prefix);
		const content = paths[key];
		for (const method in content) {
			const pretreatment = content[method];
			const operationId = _.camelCase(`${method} ${prefix}`);
			const {
				summary, parameters, responses
			} = pretreatment;
			dataSchema[operationId] = {
				url: opts.proxy.concat(key),
				operationId,
				method,
				summary,
				parameters,
				response: responses ? responses['200'] : 'any'
			};
		}
	}

	return dataSchema;
}
