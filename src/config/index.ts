import {
	api2format, dts2format, http2format
} from '../format';

const Sw2tsConf: Sw2tsType = {
	namespace: [],
	proxy: '',
	required: false,
	api: 'example/service/api/index.ts',
	http: 'example/service/request.ts',
	local: false,
	dts: 'example/typings/swagger/index.d.ts',
	hasContext: new Set<string>(),
	requireCtx: new Set<string>(),
};

const defaultConf = {
	path: 'example',
	sapce: [],
	file: 'index',
	required: false,
};

const compose = (config: ConfigSw2ts, proxy = '') => {
	return config.namespace.map((v) => proxy.concat(v));
};

export function GetSw2ts () {
	return Sw2tsConf;
}

export const eslintConfig = {
	env: {
		browser: true,
		es2021: true,
	},
	extends: ['standard'],
	parserOptions: {
		ecmaVersion: 12,
		sourceType: 'module',
	},
	parser: '@typescript-eslint/parser',
	rules: {
		camelcase: 'off',
		quotes: ['error', 'single'],
		semi: ['error', 'always'],
		indent: ['error', 'tab'],
		'no-unused-vars': 'off',
		'no-use-before-define': 'off',
		'valid-typeof': 'off',
		'no-useless-call': 'off',
		'linebreak-style': ['error', 'unix'],
		'space-before-function-paren': ['error', 'always'],
	},
};

/**
 * https://prettier.io/docs/en/options.html
 */
export const prettierOptions = {
	printWidth: 120,
	tabWidth: 2,
	useTabs: false,
	semi: false,
	singleQuote: true,
	parser: 'babel-ts',
	proseWrap: 'never',
	bracketSpacing: true,
};

export const Special = ['Integer', 'integer', 'number'];

export const parameterStore = {
	body: Object.create(null),
	path: Object.create(null),
	query: Object.create(null),
};

export function replyStore (): ParamStoreSpec {
	return {
		query: {},
		body: {},
		path: {},
		formData: {},
	};
}

export function setConfSw2ts (config: ConfigSw2ts) {
	const options = Object.assign(defaultConf, config);

	Sw2tsConf.proxy = options.proxy ?? '';
	Sw2tsConf.required = options.required ?? false;
	if (options.namespace?.length) {
		Sw2tsConf.local = true;
		Sw2tsConf.namespace = compose(options, options.proxy);
	}

	Sw2tsConf.api = api2format(options.file, options.path);
	Sw2tsConf.dts = dts2format(options.file, options.path);
	Sw2tsConf.http = http2format(options.file, options.path);
}
